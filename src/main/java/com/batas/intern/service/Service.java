package com.batas.intern.service;

import com.batas.intern.entities.Student;
import java.util.List;

/** @author anilmaharjan on 2021-04-07 */
public interface Service {
  int add();

  Student getById();

  List<Student> fetchAll();
}
