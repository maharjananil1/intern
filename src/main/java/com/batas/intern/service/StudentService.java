package com.batas.intern.service;

import com.batas.intern.entities.Student;
import java.util.ArrayList;
import java.util.List;

/** @author anilmaharjan on 2021-04-07 */
public class StudentService implements Service {
  private List<Student> students;

  public StudentService() {
    this.students = new ArrayList<>();
  }

  @Override
  public int add() {
    return 0;
  }

  @Override
  public Student getById() {
    return null;
  }

  @Override
  public List<Student> fetchAll() {
    return null;
  }
}
